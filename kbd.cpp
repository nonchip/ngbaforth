#include "kbd.hpp"

#include "gba_helpers.hpp"

#include <array>
#include <bit>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <gba/gba.hpp>
#include <string>
#include <tonc.h>
#include <tonc_surface.h>
#include <tonc_tte.h>
#include <tonc_types.h>

// font
#include "yesh1font.h"

using namespace gba;
using namespace std;

struct KBD_t KBD;

namespace {
	constexpr int font_h     = 8;
	constexpr int font_w     = 4;
	constexpr int keyboard_c = 25;
	constexpr int keyboard_r = 4;
	constexpr int keyboard_w = keyboard_c * font_w;
	constexpr int keyboard_h = keyboard_r * font_h;
	constexpr char keyboard_lower_s[]
	 = "1 2 3 4 5 6 7 8 9 0 - = `\n"
	   "q w e r t y u i o p [ ] \\\n"
	   "a s d f g h j k l ; ' A B\n"
	   "z x c v b n m , . / C D E";
	constexpr char keyboard_upper_s[]
	 = "! @ # $ % ^ & * ( ) _ + ~\n"
	   "Q W E R T Y U I O P { } |\n"
	   "A S D F G H J K L : \" F G\n"
	   "Z X C V B N M < > ? H I J";

	IWRAM_DATA uint16_t keyboard_bmp[keyboard_w * keyboard_h];

	TSurface keyboard_surf;

	static_assert(size(keyboard_lower_s) == (keyboard_c + 1 /* \n and \0 */) * keyboard_r,
	              "SIZE MISMATCH: keyboard_lower_s");
	static_assert(size(keyboard_upper_s) == (keyboard_c + 1 /* \n and \0 */) * keyboard_r,
	              "SIZE MISMATCH: keyboard_upper_s");
}

KBD_t::KBD_t() : row(0), col(0) {
	srf_init(&keyboard_surf, SRF_BMP16, reinterpret_cast<u8 *>(keyboard_bmp), keyboard_w, keyboard_h, 16, NULL);
}

bool render_buf(bool upper) {
	static int8_t lastsurf     = -1;
	const uint16_t special_col = std::bit_cast<uint16_t>(bgr555_t{5, 5, 5});

	if (lastsurf != upper) {
		lastsurf = upper;
		tte_init_bmp(3, &yesh1Font, bmp16_drawg_t);
		TTC *tc          = tte_get_context();
		tc->marginRight  = keyboard_w;
		tc->marginBottom = keyboard_h;

		tte_set_surface(&keyboard_surf);
		tte_set_pos(0, 0);
		tte_erase_screen();
		sbmp16_rect(&keyboard_surf, font_w * 22, font_h * 2, font_w * 25, font_h * 3 - 1, special_col);
		sbmp16_rect(&keyboard_surf, font_w * 20, font_h * 3, font_w * 25, font_h * 4 - 1, special_col);
		tte_write(upper ? keyboard_upper_s : keyboard_lower_s);

		// set back!
		tte_init_bmp(3, &yesh1Font, bmp16_drawg);

		return true;
	}
	return false;
}

void KBD_t::draw(int x, int y, bool force) {
	static int oxf, oyf;
	if (!visible) return;

	int xf = x + col * font_w * 2;
	int yf = y + row * font_h;

	if (render_buf(upper) || oxf != xf || oyf != yf || force) {
		sbmp16_frame(&m3_surface, x - 1, y - 1, x + keyboard_w + 1, y + keyboard_h + 1, CLR_BLACK);
		sbmp16_blit(&m3_surface, x, y, keyboard_w, keyboard_h, &keyboard_surf, 0, 0);
		sbmp16_frame(&m3_surface, xf - 1, yf - 1, xf + font_w + 1, yf + font_h + 1, CLR_ORANGE);
		oxf = xf;
		oyf = yf;
	}
}

signed char KBD_t::key(gba::keystate in) {
	//	if (in.pressed(gba::key::select)) visible = !visible;
	if (!visible) return 0; // we're invisible!

	if (in.pressed(gba::key::left)) col--;
	if (in.pressed(gba::key::right)) col++;
	if (in.pressed(gba::key::up)) row--;
	if (in.pressed(gba::key::down)) row++;

	if (col < 0) col = keyboard_c / 2;
	if (row < 0) row = keyboard_r - 1;
	if (col > keyboard_c / 2) col = 0;
	if (row >= keyboard_r) row = 0;

	if (in.pressed(gba::key::a)) {
		if (upper) {
			if (row == 2 && col == 11) return -6;
			if (row == 2 && col == 12) return -7;
			if (row == 3 && col == 10) return -8;
			if (row == 3 && col == 11) return -9;
			if (row == 3 && col == 12) return -10;
		} else {
			if (row == 2 && col == 11) return -1;
			if (row == 2 && col == 12) return -2;
			if (row == 3 && col == 10) return -3;
			if (row == 3 && col == 11) return -4;
			if (row == 3 && col == 12) return -5;
		}
		return (upper ? keyboard_upper_s : keyboard_lower_s)[row * (keyboard_c + 1) + col * 2];
	}
	if (in.pressed(gba::key::b)) return '\b'; // Backspace
	if (in.pressed(gba::key::l)) upper = !upper;
	if (in.pressed(gba::key::r)) return ' '; // Space
	if (in.pressed(gba::key::start)) return '\n';
	return 0;
}
