#pragma once
#include <cstdint>

#define save_screen_size  1200
#define gamepak_sram_size 0x8000
#define gamepak_slot_size 1213
#define gamepak_slot_num  (gamepak_sram_size / gamepak_slot_size)

bool save(uint8_t (&buffer)[save_screen_size + 1], int slot);
bool load(uint8_t (&buffer)[save_screen_size + 1], int slot);
bool clear(int slot);
