# nGBAforth

An interactive Forth for Gameboy Advance.

## Building
see `CMakeLists.txt`

## Usage

Please refer to the instructions shown on startup or in `sram_reset` in [`main.cpp`](/main.cpp).

Number in the top-right is the current stack depth, below that are the first few stack entries.

Keyboard mapping:

* `A` inserts character
* `B` deletes last character (Backspace)
* `L` shifts between lower/upper case
* `R` inserts space
* arrows to navigate
* `start` executes current input (Enter)
* `select` toggles keyboard visibility

the special `ABCDE`/`FGHJ` "keys" in the bottom right corner will insert ` [A]` to ` [J]` respectively, so you can define those for "quick access".

for details about available Words, or implementing your own, etc see `forth.cpp`.

## Examples

```forth
: Three 1 2 2 * - ;
: Two 32 64 / ;
Three Two + .
```
this should output:
```
<Number>(5)
```
while returning to an empty stack.
