//#include "ncxxforth/clang.hpp"
#include "editor.hpp"

#include "forth.hpp"
#include "gba_helpers.hpp"
#include "kbd.hpp"
#include "save.hpp"

#include <cstdint>
#include <cstdio>
#include <functional>
#include <gba/gba.hpp>
#include <numeric>
#include <string>
#include <tonc.h>
#include <tonc_input.h>
#include <tonc_tte.h>
#include <tonc_types.h>
// font
#include "yesh1font.h"

using namespace gba;
using namespace std;

namespace {

	static uint8_t screen[save_screen_size + 1];
	static uint lines_drawn = 0;
	static uint cur_slot    = 0;
	static int8_t sx = 0, sy = 0;

	void load_screen(int slot) {
		lines_drawn = 0;
		sx          = 0;
		sy          = 0;
		cur_slot    = slot;
		load(screen, cur_slot);
		for (int i = 0; i < save_screen_size; i++) // sanitize!
			if (screen[i] == 0 || screen[i] == 0xff || screen[i] == '\n') screen[i] = ' ';
	}

	void save_screen() { save(screen, cur_slot); }
};

void editor_init() {
	load_screen(0);
	KBD.visible = false;
}

void editor_exit() {
	save_screen();
	KBD.visible = true;
}

void editor(int frame, keystate &keypad_mgr) {
	TTC *tc = tte_get_context();
	if (lines_drawn < 20) {
		tc->cursorY = lines_drawn * 8;
		for (int col = 0; col < 60; col++) {
			//	tte_putc(screen[lines_drawn * 60 + col]);
			tc->cursorX = col * 4;
			tc->drawgProc(tte_get_glyph_id(screen[lines_drawn * 60 + col]));
		}
		lines_drawn++;
	} else {

		if (keypad_mgr.pressed(gba::key::select)) {
			KBD.visible = !KBD.visible;
			if (!KBD.visible) lines_drawn = 0;
			return;
		}
		if (KBD.visible) {
			signed char key = KBD.key(keypad_mgr);
			if (key > 0 && key != '\b' && key != '\n') {
				screen[sy * 60 + sx] = key;
				tc->cursorY          = sy * 8;
				tc->cursorX          = sx * 4;
				tc->drawgProc(tte_get_glyph_id(key));
				sx++;
				if (sx > 59) {
					sx = 0;
					sy += 1;
					if (sy > 19) sy = 0;
				}
				sbmp16_hline(&m3_surface, sx * 4, sy * 8 + 7, sx * 4 + 3, CLR_ORANGE);
			} else {
				static uint8_t okx = 139;
				static uint8_t oky = 126;
				uint8_t kbd_x      = sx < 30 ? 139 : 1; // "evade" cursor
				uint8_t kbd_y      = sy < 10 ? 126 : 1;
				if (okx != kbd_x || oky != kbd_y) { // moved, redraw!
					lines_drawn = 0;
					okx         = kbd_x;
					oky         = kbd_y;
				} else
					KBD.draw(kbd_x, kbd_y);
			}
		} else {
			if (keypad_mgr.pressed(gba::key::l) && cur_slot > 0) {
				save_screen();
				return load_screen(cur_slot - 1);
			} else if (keypad_mgr.pressed(gba::key::r) && cur_slot < gamepak_slot_num - 1) {
				save_screen();
				return load_screen(cur_slot + 1);
			}

			uint8_t osx = sx;
			uint8_t osy = sy;
			if (frame % 6 == 0) {
				if (keypad_mgr.held(gba::key::left)) sx--;
				if (keypad_mgr.held(gba::key::right)) sx++;
			}
			if (frame % 12 == 0) {
				if (keypad_mgr.held(gba::key::up)) sy--;
				if (keypad_mgr.held(gba::key::down)) sy++;
			}

			if (sx < 0) sx = 59;
			if (sy < 0) sy = 19;
			if (sx > 59) sx = 0;
			if (sy > 19) sy = 0;

			if (osx != sx || osy != sy) {
				if (osx < 60 && osy < 20) {
					tc->cursorY = osy * 8;
					tc->cursorX = osx * 4;
					tc->drawgProc(tte_get_glyph_id(screen[osy * 60 + osx]));
				}
			}
			sbmp16_hline(&m3_surface, sx * 4, sy * 8 + 7, sx * 4 + 3, CLR_ORANGE);
		}
	}
}
