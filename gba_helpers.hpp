#pragma once
#include "constptr.hpp"

#include <gba/gba.hpp>
#include <type_traits>
#include <version>
#define _fold(x) (__builtin_constant_p(x) ? (x) : (x))

namespace gba {

	namespace rawmem {

		constexpr volptr<uint16_t, 0x05000000> palette_memory;
		constexpr volptr<uint8_t, 0x06000000> bitmap_memory;
		constexpr volptr<uint16_t, 0x06000000> bitmap_memory_16;
		constexpr volptr<uint8_t, 0x07000000> object_memory;

		constexpr volptr<uint8_t, 0x0E000000> sram_memory;
	}

	struct bgr555_t {
			uint16_t red : 5;
			uint16_t green : 5;
			uint16_t blue : 5;
	};

	namespace io {

		constexpr volatile uint8_t *bitmap_page(bool page) noexcept { return &rawmem::bitmap_memory[page * 0xA000]; }
	}
}
