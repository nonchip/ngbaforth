cmake_minimum_required(VERSION 3.30)
# GBA TOOLCHAIN
# make sure to set -DCMAKE_TOOLCHAIN_FILE=/.../gba-toolchain/cmake/gba.toolchain.cmake
# see https://github.com/felixjones/gba-toolchain
# in VSCode:
#   - "Edit User-local CMake kits"
#   - add to the top level array:
#     {
#	       "name": "gba-toolchain",
#	       "toolchainFile": "/<path/to/your/copy/of>/gba-toolchain/cmake/gba.toolchain.cmake"
#     }

# shut cmake up
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

#SET(CMAKE_SYSTEM_NAME Generic)
#SET(CMAKE_SYSTEM_PROCESSOR arm)
#SET(CMAKE_CROSSCOMPILING 1)
#set(CMAKE_C_COMPILER_WORKS 1)
#set(CMAKE_CXX_COMPILER_WORKS 1)
#set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
#set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS "")
#set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "")

# PROJECT

project(ngbaforth ASM C CXX)

# BUILD PROCESS

set(target ngbaforth)
set(ROM_TITLE "NGBAFORTH")
set(ROM_GAME_CODE "NFTH")
set(ROM_MAKER_CODE "NC")
set(ROM_VERSION 000)

add_executable(${target}
	main.cpp
	yesh1font.s
	kbd.cpp
	terminal.cpp
	editor.cpp
	save.cpp
	forth.cpp
	ncxxforth/runtime.cpp
)

set_property(TARGET ${target} PROPERTY CXX_STANDARD 20)
#gba_target_sources_instruction_set(${target} thumb)
target_compile_options(${target} PRIVATE -fno-exceptions -mthumb -fconserve-stack -fomit-frame-pointer)
#set_target_properties(${target} PROPERTIES SUFFIX ".elf")
#gba_target_object_copy(${target} "${target}.elf" "${target}.gba")
#gba_target_fix(${target} "${target}.gba" "${ROM_TITLE}" "${ROM_GAME_CODE}" "${ROM_MAKER_CODE}" ${ROM_VERSION})

find_package(agbabi REQUIRED)
find_package(gba-hpp REQUIRED)
find_package(posprintf REQUIRED)
find_package(tonclib REQUIRED)
find_package(librom REQUIRED)

#gba_target_link_agb_abi(${target})
#gba_target_link_gba_plusplus(${target})
#gba_target_link_posprintf(${target})
#gba_target_link_tonc(${target})

set_target_properties(${target} PROPERTIES
ROM_TITLE ${ROM_TITLE}
ROM_ID ${ROM_GAME_CODE}
ROM_MAKER ${ROM_MAKER_CODE}
ROM_VERSION ${ROM_VERSION}
)

target_link_libraries(${target} PRIVATE agbabi gba-hpp posprintf tonclib librom)

target_link_options(${target} PRIVATE
	LINKER:-Map,${CMAKE_CURRENT_BINARY_DIR}/${target}.map
)

#gba_target_link_runtime(${target} rom)
#gba_target_link_runtime( ${target} flashcart FLASH1M_Vnnn )

install_rom(${target})
