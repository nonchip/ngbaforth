#pragma once
#include <gba/gba.hpp>

void editor(int frame, gba::keystate &keypad_mgr);
void editor_init();
void editor_exit();
