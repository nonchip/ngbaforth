#include "save.hpp"

#include "gba_helpers.hpp"

using namespace gba;

bool save(uint8_t (&buffer)[save_screen_size + 1], int slot) {
	uint8_t sum              = 0;
	buffer[save_screen_size] = 0;
	for (int i = 0; i < save_screen_size; i++) {
		uint8_t byte                                      = buffer[i];
		rawmem::sram_memory[i + gamepak_slot_size * slot] = byte;
		sum += byte - i;
	}
	rawmem::sram_memory[save_screen_size + gamepak_slot_size * slot] = sum;

	return true; // TODO: actually check?
}

bool clear(int slot) {
	uint8_t sum = 0;
	for (int i = 0; i < save_screen_size; i++) {
		uint8_t byte                                      = ' ';
		rawmem::sram_memory[i + gamepak_slot_size * slot] = byte;
		sum += byte - i;
	}
	rawmem::sram_memory[save_screen_size + gamepak_slot_size * slot] = sum;
	return true; // TODO: actually check?
}

bool load(uint8_t (&buffer)[save_screen_size + 1], int slot) {
	uint8_t sum = 0;
	for (int i = 0; i < save_screen_size; i++) {
		uint8_t byte = rawmem::sram_memory[i + gamepak_slot_size * slot];
		buffer[i]    = byte;
		sum += byte - i;
	}
	buffer[save_screen_size] = 0;
	return sum == rawmem::sram_memory[save_screen_size + gamepak_slot_size * slot];
}
