//#include "ncxxforth/clang.hpp"
#include "editor.hpp"
#include "forth.hpp"
#include "gba_helpers.hpp"
#include "kbd.hpp"
#include "ncxxforth/ncxxforth.config.hpp"
#include "save.hpp"
#include "terminal.hpp"

#include <cstdint>
#include <cstdio>
#include <functional>
#include <gba/gba.hpp>
#include <numeric>
#include <string>
#include <tonc.h>
#include <tonc_tte.h>
#include <tonc_types.h>
// font
#include "yesh1font.h"

using namespace std;
extern string out_str;
extern string in_str;

#ifdef DEBUG
#define LOG gba::mgba::printf
#else
#define LOG //
#endif

void sram_reset() { // init save slot 0
	static uint8_t buf[][save_screen_size + 1] = {
	 "( \\60 columns                                              )"
	 "( 2                                                        )"
	 "( 0    nGBAforth v000                                      )"
	 "(      Copyright 2022-2025 Kyra 'nonchip' Zimmer           )"
	 "( r  ----------------------------------------              )"
	 "( o                                                         "
	 "( w    This is the 'script' editor.                         "
	 "( s    The script consists of up to 27 screens of Forth,    "
	 "       which you can edit and which run at startup.         "
	 "       Swap between these screens with L / R.               "
	 "       This makes it very useful for common functions,      "
	 "       or even full games. Unused space can be used to      "
	 "       store data, execution ends on the first empty screen."
	 "                                                            "
	 "       There is also a 'command line' and a 'game mode'.    "
	 "       The command line automatically runs when you exit    "
	 "       this script editor by pressing START.                "
	 "       You can swap between command line and game mode      "
	 "       at any point by pressing SELECT.                     "
	 "                                    Press R now to read on!)",
	 "( Editor Controls:                                          "
	 "      DPad   moves the cursor                               "
	 "      L/R    swap between screens                           "
	 "      START  exits editor, runs script, enters command line "
	 "      SELECT toggles keyboard                              )"
	 "( Command Line Controls:                                    "
	 "      START  executes current input                         "
	 "      SELECT switches to game mode                          "
	 "  The current Stack size and contents are shown in the top- "
	 "    right corner, the keyboard will move out of your way.  )"
	 "( Game Mode Controls:                                       "
	 "      SELECT switches to command line                       "
	 "  In game mode, each frame the runtime will try to execute  "
	 "  the word !FRAME! , supplying on the stack:                "
	 "    - the current frame number, on top                      "
	 "    - a bitmask number, below, of the input button states,  "
	 "      in LSB-first order of:                                "
	 "          A,B,SELECT,START,RIGHT,LEFT,UP,DOWN,R,L           "
	 "  It is expected to DROP those 2 values and return nothing.)"
	 "                                                       (R->)",
	 "( Here's a fun example 'game': )                            "
	 ": !FRAME! ( keys frame -- )                                 "
	 "  ( first, some housekeeping: )                             "
	 "    SWAP            ( frame keys )                          "
	 "    DROP            ( frame )                               "
	 "    0x7FFF          ( frame 'white' )                       "
	 "    SWAP            ( 'white' frame )                       "
	 "  ( wrap the frame by the screen size to get a valid pixel )"
	 "    $VS %           ( 'white' pixel=frame%size )            "
	 "  ( and multiply by 2 to get an address offset for VRAM )   "
	 "    2 *             ( 'white' offset=pixel*2 )              "
	 "  ( add VRAM address to get an actual valid address )       "
	 "    $VRAM +         ( 'white' address=offset+VRAM )         "
	 "  ( convert it to a pointer )                               "
	 "    =*              ( 'white' pointer=*address )            "
	 "  ( perform a 16bit write )                                 "
	 "    !!              ( )                                     "
	 ";                                                           "
	 "( This 'game' will slowly fill the screen white by setting  "
	 "  one pixel every frame, corresponding to the frame count. )",
	};

	for (int i = 0; i < gamepak_slot_num; i++) clear(i);

	for (int i = 0; i < std::size(buf); i++) save(buf[i], i);

	gba::bios::RegisterRamReset(gba::bios::ram_reset{true});
	gba::bios::SoftReset();

	__builtin_unreachable();
}

const static ncxxforth::tcell::String gamesword("!FRAME!");
bool game(int frame, gba::keystate &keypad_mgr) {
	if (FORTH.dict.contains(gamesword)) {
		int istack = FORTH.stack.size();
		FORTH.stack.push((ncxxforth::tcell::Number)((uint32_t)(bit_cast<uint32_t>(keypad_mgr))));
		FORTH.stack.push((ncxxforth::tcell::Number)frame);
		FORTH.eval(gamesword);
		return istack == FORTH.stack.size();
	}
	return false;
}

int main() {

	{
		using namespace gba;
		#ifdef DEBUG
		mgba::open();
		LOG(mgba::log::info, "mgba logging enabled");
		#endif

		mmio::WAITCNT = waitcnt_default;
		mmio::MEMCNT = memcnt_default;

		bios::RegisterRamReset(bios::ram_reset{.palette = true, .vram = true, .oam = true});

		dispcnt disp_mode = {.video_mode = 3, .hblank_oam_free = true, .show_bg2 = true};
		mmio::DISPCNT = disp_mode;
		tte_init_bmp(3, &yesh1Font, bmp16_drawg);

		mmio::IRQ_HANDLER = agbabi::irq_empty;
		mmio::DISPSTAT = {.irq_vblank = true};
		mmio::IE = {.vblank = true};
		mmio::IME = true;

		LOG(mgba::log::info, "hardware init done");
	}

	int frame = 0;
	gba::keystate keypad_mgr;

	{

		{ // verify slot 0

			uint8_t buf[save_screen_size + 1];
			if (!load(buf, 0)) {
				LOG(gba::mgba::log::error, "could not load anything!");
				sram_reset();
			}
		}
	}

	LOG(gba::mgba::log::info, "sram slot 0 verified");

	editor_init();

	LOG(gba::mgba::log::info, "editor init done, running main loop");

	enum current_view_t { ve, vt, vg };
	enum current_view_t current_view = ve;
	while (true) {
		gba::bios::VBlankIntrWait();
		keypad_mgr = *gba::mmio::KEYINPUT;

		LOG(gba::mgba::log::info, "vblank, input %x", keypad_mgr);

		//try {
			switch (current_view) {
				case ve:
					if (keypad_mgr.pressed(gba::key::start)) { // exit editor now
						editor_exit();
						current_view = vt;
						frame        = -1;

						uint8_t buf[save_screen_size + 1];
						int i;
						for (i = 0; i < gamepak_slot_num; i++) {
							if (!load(buf, i)) {
								LOG(gba::mgba::log::error, "could not load slot %02x!", i);
								sram_reset();
							}

							string str{reinterpret_cast<char *>(buf), save_screen_size};
							if (str.find_first_not_of(string{ncxxforth::consts::svDELIMS} + "\xff\x00"s) == std::string::npos) break;
							FORTH.eval(str);
						}
						LOG(gba::mgba::log::info, "loaded %02x slots!", i);
					} else { // show editor
						editor(frame, keypad_mgr);
					}
					break;
				case vt:
					terminal(frame, keypad_mgr);
					if (keypad_mgr.pressed(gba::key::select)) {
						current_view = vg; // switch will happen on next frame
						frame        = -1;
					}
					break;
				case vg:
					bool ret = game(frame, keypad_mgr);
					if (keypad_mgr.pressed(gba::key::select) || !ret) {
						current_view = vt; // switch will happen on next frame
						frame        = -1;
					}
					break;
			}
		//} catch (const exception &e) {
		//	LOG(gba::mgba::log::fatal, "Exception: %s", e.what());
		//}
		frame++;
	}
	LOG(gba::mgba::log::fatal, "Reached the unreachable!");
	__builtin_unreachable();
}
