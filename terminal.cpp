//#include "ncxxforth/clang.hpp"
#include "forth.hpp"
#include "gba_helpers.hpp"
#include "kbd.hpp"

#include <cstdint>
#include <cstdio>
#include <functional>
#include <gba/gba.hpp>
#include <numeric>
#include <string>
#include <tonc.h>
#include <tonc_tte.h>
#include <tonc_types.h>
#include <tonc_video.h>
// font
#include "yesh1font.h"

//using namespace gba;
using namespace std;
string out_str;
string in_str;

void terminal(int frame, gba::keystate &keypad_mgr) {
	static bool curs = false;
	static int ix, iy;
	static int cx, cy;

	bool re_out     = (frame == 0);
	bool re_in      = (frame == 0);
	signed char key = KBD.key(keypad_mgr);
	if (key != 0) {
		switch (key) {
			case -1: // special keys
			case -2:
			case -3:
			case -4:
			case -5:
			case -6:
			case -7:
			case -8:
			case -9:
			case -10:
				in_str += " [" + string{static_cast<char>('A' - key - 1)} + "]";
				re_in = true;
				break;
			case '\b': // backspace
				if (in_str.length()) in_str.pop_back();
				re_in = true;
				break;
			case '\n': // exec
				in_str += key;
				out_str += in_str;
				FORTH.eval(in_str);
				in_str.clear();
				re_out = true;
				break;
			default: // normal char
				in_str += key;
				re_in = true;
		}
	}
	if (re_out) {
		tte_erase_screen();
		tte_set_pos(0, 0);
		tte_write(out_str.c_str());
		tte_get_pos(&ix, &iy);
		while (iy + 16 > M3_HEIGHT) {
			out_str.erase(0, out_str.find("\n") + 1);
			tte_erase_screen();
			tte_set_pos(0, 0);
			tte_write(out_str.c_str());
			tte_get_pos(&ix, &iy);
		}
		re_in = true;
	}

	if (re_in) {
		tte_erase_rect(ix, iy, M3_WIDTH, M3_HEIGHT);
		tte_set_pos(ix, iy);
		tte_write(in_str.c_str());
		tte_get_pos(&cx, &cy);
	}

	if (frame % 60 == 0) {
		tte_erase_rect(cx, cy, cx + 4, cy + 8);
		tte_set_pos(cx, cy);
		if (curs) tte_putc('_');
		curs = !curs;
	}

	string s = "[ "s + std::to_string(FORTH.stack.size()) + " ]"s;
	tte_set_pos(M3_WIDTH - s.length() * 4, 0);
	tte_write(s.c_str());
	if (FORTH.stack.size() > 0) {
		auto st = FORTH.stack;
		for (int y = 8; !st.empty() && y < 100; y += 8) {
			s = std::to_string(st.top());
			st.pop();
			if (s.length() > 25) {
				s.resize(22);
				s += "...";
			}
			tte_set_pos(M3_WIDTH - s.length() * 4, y);
			tte_write(s.c_str());
		}
	}
	KBD.visible = true;
	KBD.draw((iy < M3_HEIGHT / 2) ? 139 : 1, (iy < M3_HEIGHT / 2) ? 126 : 1, re_out || re_in);
}
