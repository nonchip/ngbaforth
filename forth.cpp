#include "forth.hpp"

#include <gba/gba.hpp>
#include "gba_helpers.hpp"
#include "ncxxforth/ncxxforth.config.hpp"

using namespace ncxxforth;
using namespace std;
using namespace std::literals;

extern string out_str;
extern int save_ptr;
extern void sram_reset();

namespace {
	static Word stack_one{{
	                       [](Forth &f) {
		                       if (f.stack.size() < 1) {
			                       //			                       cerr << "Stack empty!" << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word stack_drop{{
	                        &stack_one,
	                        [](Forth &f) { f.stack.pop(); },
	                       },
	                       false};

	static Word stack_num{{
	                       &stack_one,
	                       [](Forth &f) {
		                       if (!holds_alternative<tcell::Number>(f.stack.top())) {
			                       //			                       cerr << "Expected Number: " << to_string(f.stack.top()) << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};
	static Word stack_str{{
	                       &stack_one,
	                       [](Forth &f) {
		                       if (holds_alternative<tcell::String>(f.stack.top())) return;
		                       if (holds_alternative<tcell::StringView>(f.stack.top())) return;
		                       //			                       cerr << "Expected Number: " << to_string(f.stack.top()) << endl;
		                       f.breaks = 2;
	                       },
	                      },
	                      false};

	static Word stack_ptr{{
	                       &stack_one,
	                       [](Forth &f) {
		                       if (!holds_alternative<tcell::NativePtr>(f.stack.top())) {
			                       //			                       cerr << "Expected Number: " << to_string(f.stack.top()) << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word stack_min{{
	                       &stack_one,
	                       &stack_num,
	                       [](Forth &f) {
		                       auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                       f.stack.pop();
		                       if (f.stack.size() < i) {
			                       //			                       cerr << "Expected Stack >= " << i << endl;
			                       f.breaks = 2;
		                       }
	                       },
	                      },
	                      false};

	static Word pop_to_local{{
	                          Cell((tcell::Number)2),
	                          &stack_min,
	                          &stack_num,
	                          [](Forth &f) {
		                          auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                          f.stack.pop();
		                          f.exec_parent->content[i] = f.stack.top();
		                          f.stack.pop();
	                          },
	                         },
	                         false};

	static Word push_from_local{{
	                             &stack_one,
	                             &stack_num,
	                             [](Forth &f) {
		                             auto i = static_cast<size_t>(get<tcell::Number>(f.stack.top()));
		                             f.stack.pop();
		                             f.stack.push(f.exec_parent->content[i]);
	                             },
	                            },
	                            false};
	static Word skip_n{{
	                    &stack_one,
	                    &stack_num,

	                    [](Forth &f) {
		                    f.breaks = 1;
		                    f.jumps  = get<tcell::Number>(f.stack.top()) + 1;
		                    f.stack.pop();
	                    },
	                   },
	                   true};

					   template <typename Op> static Word op1{
						{
						 Cell((tcell::Number)1),
						 &skip_n,
						 Cell(), // placeholder a
				   
						 Cell((tcell::Number)1),
						 &stack_min,
				   
						 &stack_num,
						 Cell((tcell::Number)2), // a
						 &pop_to_local,
				   				   
						 [](Forth &f) {
							 Op o;
							 f.stack.push(o(get<tcell::Number>(f.executing->content[2])));
						 },
						},
						false};

	template <typename Op> static Word op2{
	 {
	  Cell((tcell::Number)2),
	  &skip_n,
	  Cell(), // placeholder a
	  Cell(), // placeholder b

	  Cell((tcell::Number)2),
	  &stack_min,

	  &stack_num,
	  Cell((tcell::Number)2), // a
	  &pop_to_local,

	  &stack_num,
	  Cell((tcell::Number)3), // b
	  &pop_to_local,

	  [](Forth &f) {
		  Op o;
		  f.stack.push(o(get<tcell::Number>(f.executing->content[3]), get<tcell::Number>(f.executing->content[2])));
	  },
	 },
	 false};

	template <typename Op> static Word op2bo{
	 {
	  Cell((tcell::Number)2),
	  &skip_n,
	  Cell(), // placeholder a
	  Cell(), // placeholder b

	  Cell((tcell::Number)2),
	  &stack_min,

	  &stack_num,
	  Cell((tcell::Number)2), // a
	  &pop_to_local,

	  &stack_num,
	  Cell((tcell::Number)3), // b
	  &pop_to_local,

	  [](Forth &f) {
		  Op o;
		  f.stack.push(o(get<tcell::Number>(f.executing->content[3]), get<tcell::Number>(f.executing->content[2]))
		                ? consts::nTRUE
		                : consts::nFALSE);
	  },
	 },
	 false};

	static Word semi{{
	                  [](Forth &f) { f.breaks = 2; },
	                 },
	                 true};

	template <bool I> static Word colon{{
	                                     [](Forth &f) {
		                                     tcell::String name = f.read_word();
		                                     Word *word         = new Word(f.compile_inplace());
		                                     word->immediate    = I;
		                                     f.dict.insert_or_assign(name, word);
	                                     },
	                                    },
	                                    false};

	static Word period{
	 {
	  &stack_one,
	  [](Forth &f) {
		  out_str += std::visit(
		   ncxxforth::visitor{
		    [](ncxxforth::tcell::Number c) -> std::string { return std::to_string(c); },
		    [f](ncxxforth::Word *c) -> std::string {
			    ncxxforth::tcell::StringView name = f.dictFind(c);
			    return name.empty() ? std::to_string(reinterpret_cast<uintptr_t>(c)) : std::string{name};
		    },
		    [](ncxxforth::tcell::NativePtr c) -> std::string { return std::to_string(reinterpret_cast<uintptr_t>(c)); },
		    [](ncxxforth::tcell::NativeWord c) -> std::string { return std::to_string(reinterpret_cast<uintptr_t>(c)); },
		    [](const ncxxforth::tcell::String &c) -> std::string { return c; },
		    [](const ncxxforth::tcell::StringView &c) -> std::string { return std::string{c}; },
		   },
		   f.stack.top());
		  // formatting borked
	  },
	  &stack_drop,
	 },
	 false};

	static Word paren{{
	                   [](Forth &f) { f.read_word(")"); },
	                  },
	                  true};

	static Word quot{{
	                  [](Forth &f) {
		                  if (f.compiling) {
			                  f.compiling->content.push_back(f.read_word("\""));
		                  } else {
			                  f.stack.push(f.read_word("\""));
		                  }
	                  },
	                 },
	                 true};

	static Word findword{{
	                      &stack_one,
	                      [](Forth &f) {
		                      std::string sword;
		                      auto top = f.stack.top();
		                      if (holds_alternative<tcell::StringView>(top)) {
			                      sword = std::string{get<ncxxforth::tcell::StringView>(top)};
			                      f.stack.pop();
		                      } else if (holds_alternative<tcell::String>(top)) {
			                      sword = get<ncxxforth::tcell::String>(top);
			                      f.stack.pop();
		                      } else
			                      return;
		                      if (f.dict.contains(sword)) {
			                      Word *w = f.dict.at(sword);
			                      f.stack.push(w);
		                      }
	                      },
	                     },
	                     true};
	static Word bracket{{
	                     [](Forth &f) {
		                     auto code = f.read_word("]");
		                     f.eval(code);
	                     },
	                    },
	                    true};
	static Word compile{{
	                     [](Forth &f) {
		                     if (f.compiling) {
			                     auto c = f.stack.top();
			                     f.compiling->content.push_back(c);
			                     f.stack.pop();
		                     }
	                     },
	                    },
	                    true};

	static Word uncompile{{
	                       [](Forth &f) {
		                       if (f.compiling && f.compiling->content.size()) {
			                       f.stack.push(f.compiling->content.back());
			                       f.compiling->content.pop_back();
		                       }
	                       },
	                      },
	                      true};

	template <typename T> static Word peek{{
	                                        Cell((tcell::Number)1),
	                                        &skip_n,
	                                        Cell(), // placeholder a

	                                        Cell((tcell::Number)1),
	                                        &stack_min,

	                                        &stack_ptr,
	                                        Cell((tcell::Number)2), // a
	                                        &pop_to_local,
	                                        [](Forth &f) {
		                                        f.stack.push(Cell(static_cast<tcell::Number>(
		                                         *reinterpret_cast<T *>(get<tcell::NativePtr>(f.executing->content[2])))));
	                                        },
	                                       },
	                                       false};
	template <typename T> static Word poke{{
	                                        Cell((tcell::Number)2),
	                                        &skip_n,
	                                        Cell(), // placeholder a
	                                        Cell(), // placeholder b

	                                        Cell((tcell::Number)2),
	                                        &stack_min,

	                                        &stack_ptr,
	                                        Cell((tcell::Number)3), // b
	                                        &pop_to_local,

	                                        &stack_num,
	                                        Cell((tcell::Number)2), // a
	                                        &pop_to_local,
	                                        [](Forth &f) {
		                                        *(T *)get<tcell::NativePtr>(f.executing->content[3])
		                                         = (T)get<tcell::Number>(f.executing->content[2]);
	                                        },
	                                       },
	                                       false};

	template <typename T> static Word cast{{
	                                        Cell((tcell::Number)1),
	                                        &skip_n,
	                                        Cell(), // placeholder a

	                                        Cell((tcell::Number)1),
	                                        &stack_min,

	                                        &stack_num,
	                                        Cell((tcell::Number)2), // a
	                                        &pop_to_local,
	                                        [](Forth &f) {
		                                        auto c = f.executing->content[2];
		                                        if (holds_alternative<tcell::Number>(c))
			                                        f.stack.push((T)get<tcell::Number>(c));
		                                        else if (holds_alternative<tcell::NativePtr>(c))
			                                        f.stack.push((T)get<tcell::NativePtr>(c));
	                                        },
	                                       },
	                                       false};

	template <Cell &C> static Word toCell{{&stack_one, [](Forth &f) { C = f.stack.top(); }, &stack_drop}, false};
	template <Cell &C> static Word fromCell{{[](Forth &f) { f.stack.push(C); }}, false};

	static Word EXC_SRAM_RESET_EXC{{[](Forth &f) { sram_reset(); }}, false};
	static Word HELP_Q{{"HELP? not yet implemented.\n"s, &period}, false};
	static Word WORDS_Q{{"WORDS? not yet implemented.\n"s, &period}, false};

	static Word BREAK{{[](Forth &f) {
		                  f.breaks = get<tcell::Number>(f.stack.top()) + 1;
		                  f.jumps  = 1;
		                  f.stack.pop();
	                  }},
	                  false};
	static Word JUMP{{[](Forth &f) {
		                 f.breaks = 1;
		                 f.jumps  = get<tcell::Number>(f.stack.top()) + 1;
		                 f.stack.pop();
	                 }},
	                 false};

	static Word comp_count{{[](Forth &f) { f.stack.push(Cell((tcell::Number)f.compiling->content.size())); }}, true};
	static Word stack_count{{[](Forth &f) { f.stack.push(Cell((tcell::Number)f.stack.size())); }}, false};

	static Word ifnskip{{[](Forth &f) {
		                    auto n = get<tcell::Number>(f.stack.top());
		                    f.stack.pop();
		                    if (n == consts::nFALSE) {
			                    f.breaks = 1;
			                    f.jumps  = 2;
		                    }
	                    }},
	                    false};

	static Cell GLOBA, GLOBB, GLOBC;

	static Word _TRUE{{consts::nTRUE}, false};
	static Word _FALSE{{consts::nFALSE}, false};
}

// clang-format off
[[gnu::section(".iwram.forth")]] Forth FORTH{{
  {"TRUE",&_TRUE},{"FALSE",&_FALSE},

  {"+", &op2< plus<tcell::Number> >},
  {"-", &op2< minus<tcell::Number> >},
  {"*", &op2< multiplies<tcell::Number> >},
  {"/", &op2< divides<tcell::Number> >},
  {"%", &op2< modulus<tcell::Number> >},

  {"&", &op2< bit_and<tcell::Number> >},
  {"|", &op2< bit_or<tcell::Number> >},
  {"^", &op2< bit_xor<tcell::Number> >},
  {"~", &op1< bit_not<tcell::Number> >},

  {"==", &op2bo< equal_to<tcell::Number> >},
  {"!=", &op2bo< not_equal_to<tcell::Number> >},
  {"<=", &op2bo< less_equal<tcell::Number> >},
  {"<", &op2bo< less<tcell::Number> >},
  {">=", &op2bo< greater_equal<tcell::Number> >},
  {">", &op2bo< greater<tcell::Number> >},

  {":", &colon<false>},{"::", &colon<true>},{";", &semi},
  {"[", &bracket},{"[!", &compile},{"[@", &uncompile},{"'",&findword},
  {".", &period},
  {"(", &paren},
  {"\"",&quot},

  {"C#",&comp_count},
  {"S#",&stack_count},

  {"=N",&cast<tcell::Number>},
  {"=*",&cast<tcell::NativePtr>},
  {"=:",&cast<tcell::NativeWord>},
  {"!",    &poke<int8_t>}, {"@",   &peek<int8_t>},
  {"!!",   &poke<int16_t>},{"@@",  &peek<int16_t>},
  {"!!!",  &poke<int32_t>},{"@@@", &peek<int32_t>},
  {"!!!!", &poke<int64_t>},{"@@@@",&peek<int64_t>},
  {"A!",&toCell<GLOBA>},{"A@",&fromCell<GLOBA>},
  {"B!",&toCell<GLOBB>},{"B@",&fromCell<GLOBB>},
  {"C!",&toCell<GLOBC>},{"C@",&fromCell<GLOBC>},

  {"JUMP",&JUMP},{"BREAK",&BREAK},{"?",&ifnskip},

  {"!SRAM_RESET!",&EXC_SRAM_RESET_EXC},
  {"HELP?",&HELP_Q},
  {"WORDS?",&WORDS_Q},
},R"(

  : DUP ( x -- x x) A! A@ A@ ;
  : DROP ( x -- ) A! ;
  : SWAP ( x y -- y x ) A! B! A@ B@ ;

  ( -- segment_start )
  : $EWRAM 0x02000000 ;
  : $IWRAM 0x03000000 ;
  : $IOREG 0x04000000 ;

  : $VPAL 0x05000000 ;
  : $VRAM 0x06000000 ;
  : $VOAM 0x07000000 ;
  : $VW 240 ;
  : $VH 160 ;
  : $VS [ $VW $VH * ] [! ;

  : $ROM0 0x08000000 ;
  : $ROM1 0x0A000000 ;
  : $ROM2 0x0C000000 ;
  : $SRAM 0x0E000000 ;

)"};
// : test 5 SWAP ? JUMP " no" . DROP 2 JUMP " yes" . ;
// clang-format on
