#pragma once


#include <gba/gba.hpp>

struct KBD_t {
		bool upper   = false;
		bool visible = true;
		int row, col;
		KBD_t();
		void draw(int x, int y, bool force = false);
		signed char key(gba::keystate in);
};

extern struct KBD_t KBD;
